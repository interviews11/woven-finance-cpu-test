# DOCUMENTATION FOR WOVEN FINANCE API

| Nam                   	| Method 	| Endpoint       	| description                                                                                                                                    	|
|-----------------------	|--------	|----------------	|------------------------------------------------------------------------------------------------------------------------------------------------	|
| default endpoint      	| all    	| /              	| default endpoint for app                                                                                                                       	|
| Calculate hardware    	| post   	| /api/calculate 	| main test endpoint for the application. it returns the number of servers that are required to host a non-empty collection of virtual machines. 	|
| Get Server estimation 	| post   	| /api/estimate  	| get the server estimate for a given number of users                                                                                            	|
|


### Calculate hardware
***POST*** `/api/calculate`
Sample Request Payload
```json
{
      "server": { "CPU": 2, "RAM": 32, "HDD": 100 },
      "vm": [
        { "CPU": 1, "RAM": 16, "HDD": 10 },
        { "CPU": 1, "RAM": 16, "HDD": 10 },
        { "CPU": 2, "RAM": 32, "HDD": 100 },
      ],
};
```

Sample Response
```json
{
    "message": "Server requirements",
    "result": 3,
    "data": {
        "requiredServers": 3,
        "server": {
            "CPU": 45,
            "RAM": 64,
            "HDD": 200
        },
        "resource": {
            "CPU": 5,
            "RAM": 67,
            "HDD": 130
        },
        "used": {
            "cpu": 40,
            "ram": -3,
            "hdd": 70
        }
    }
}
```


### Calculate Server estimation
***POST*** `/api/estimate`
Sample Request Payload
```json
{
    "users": 200,
    "memory":  16,
    "userMemory": 128,
    "redundancy": 2
}
```

Sample Response

```json
{
    "message": "Basic Memory Estimation Requirement",
    "advice": "A minimun of 4 CPUs and 16GB memory per server is required.",
    "data": {
        "requiredMemory": "32.50",
        "cpu": 4,
        "growthPercent": 1.3,
        "servers": 5,
        "minServerMemory": 16
    }
}
```