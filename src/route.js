import express from 'express';
import { calculateCPU, calculateMemoryEstimation } from './controller';
import { calculateCPUValidator, estimationValidator } from './request-validator';

const route =  express.Router();

/**
 * calculates the server hardware for a data center (to host virtual machines).
 */
route.post(['', '/calculate'], calculateCPUValidator, calculateCPU);

/**
 * calculate the server estimate for a given number of users
 */
route.post('/estimate', estimationValidator, calculateMemoryEstimation);

export default route;
