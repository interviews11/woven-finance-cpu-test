export const calculateCPU = (req, res) => {
 const { server, vm } = req.body;
 const { CPU, RAM, HDD } = server;
 // calculate total vm resource
 const resource = { CPU: 0,
RAM: 0,
HDD: 0 };

 let requiredServers = 0;

 /*
  * sum the total resources together
  * then check if it marches the available hardware
  */
 for (const machine of vm)  {

  resource.CPU += machine.CPU;
   resource.RAM += machine.RAM;
   resource.HDD += machine.HDD;

  if (resource.CPU <= CPU && resource.RAM <= RAM && resource.HDD <= HDD) {
   ++requiredServers;
  }
}
 // divide the resource by the number of available server

 const cpu = CPU - resource.CPU;
 const ram = RAM - resource.RAM ;
 const hdd = HDD - resource.HDD;


 return res.status(200).json({
  message: 'Server requirements',
  result: requiredServers,
  data: {
   requiredServers,
   server,
   resource,
   used: { cpu,
ram,
hdd  }
  }
 });

};

export const calculateMemoryEstimation = (req, res) => {
 const { users, userMemory, memory, percent, redundancy: redLevel } = req.body;
 const estimatedUsers = users;
 const memoryUser = userMemory || 128;
 const growthPercent = (percent || 30 / 100) + 1;
 const requiredMemory = ((estimatedUsers * memoryUser * growthPercent) / 1024).toFixed(2);

 const maxMemory = memory || 16;
 const redundancy = redLevel || 2;

 const estimatedLevel = requiredMemory * redundancy;
 const servers = Math.ceil(estimatedLevel / maxMemory);
 const divisible = 8;
 const minServerMemory = Math.ceil((estimatedLevel / servers) / divisible) * divisible ;
 const cpu = Math.floor(minServerMemory / 4);

 return res.status(200).json({
  message: 'Basic Memory Estimation Requirement',
  advice: `A minimun of ${cpu} CPUs and ${minServerMemory}GB memory per server is required.`,
  data: {
   requiredMemory,
   cpu,
   growthPercent,
   servers,
   minServerMemory,
  }
 });
};
