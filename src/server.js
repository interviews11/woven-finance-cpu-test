/* eslint-disable no-undef */
import "regenerator-runtime/runtime.js";
import app from './app';
const PORT = process.env.PORT || 7003;

app.listen(PORT,   function () {
    const server = `http://localhost:${PORT}`;
    // eslint-disable-next-line no-console
    console.log('Server Started ON:',  server);
});

export default app;
