/* eslint-disable no-undef */
import supertest from "supertest";
import { assert } from "chai";
import app from "../app";

const request = supertest(app);

describe("APP SERVER /", () => {
  it("server should run successfully", (done) => {
    request.get("/").expect(["powered by EmmsDan"], done);
  });
  it("return 404 when wrong url is entered", (done) => {
    request
      .get("/some-fun-url")
      .expect(["Oops, url not found on this app"], done);
  });
});

describe("Calculate Server Hardware '/servers/calculate' and '/api/' ", () => {
  it("Fail validation on empty payload", (done) => {
    request
      .post("/api/")
      .send({})
      .expect("Content-Type", /json/u)
      .expect(JSON.stringify({ error: '"server" is required' }))
      .expect(402, done);
  });
  it("Fail validation on empty payload 2", (done) => {
    request
      .post("/api/calculate")
      .send({})
      .expect("Content-Type", /json/u)
      .expect(JSON.stringify({ error: '"server" is required' }))
      .expect(402, done);
  });

  it("Fail validation on none object payload", (done) => {
    request
      .post("/api/")
      .send({ server: "" })
      .expect("Content-Type", /json/u)
      .expect(JSON.stringify({ error: '"server" must be of type object' }))
      .expect(402, done);
  });

  it("Should calculate Server hardware correctly", (done) => {
    const payload = {
      server: { CPU: 2, RAM: 32, HDD: 100 },
      vm: [
        { CPU: 1, RAM: 16, HDD: 10 },
        { CPU: 1, RAM: 16, HDD: 10 },
        { CPU: 2, RAM: 32, HDD: 100 },
      ],
    };
    const expectedResp = {
      message: "Server requirements",
      result: 2,
      data: {
        requiredServers: 2,
        server: { CPU: 2, RAM: 32, HDD: 100 },
        resource: { CPU: 4, RAM: 64, HDD: 120 },
        used: { cpu: -2, ram: -32, hdd: -20 },
      },
    };
    request
      .post("/api/")
      .send(payload)
      .expect("Content-Type", /json/u)
      .expect(expectedResp)
      .expect(200, done);
  });
  it("Should calculate Server hardware correctly as 3", (done) => {
    const payload = {
      server: { CPU: 4, RAM: 64, HDD: 130 },
      vm: [
        { CPU: 1, RAM: 16, HDD: 10 },
        { CPU: 1, RAM: 16, HDD: 10 },
        { CPU: 2, RAM: 16, HDD: 100 },
        { CPU: 2, RAM: 32, HDD: 100 },
      ],
    };
    const expectedResp = { result: 3 };
    request
      .post("/api/")
      .send(payload)
      .expect("Content-Type", /json/u)
      .expect((res) => {
        const respose = res.body;
        assert.containsAllDeepKeys(respose, expectedResp);
      })
      .expect(200, done);
  });
});

describe("Calculate memory estimation '/servers/estimate' ", () => {
  it("Validation: isEmpty payload", (done) => {
    request
      .post("/api/estimate")
      .send({})
      .expect("Content-Type", /json/u)
      .expect(JSON.stringify({ error: '"users" is required' }))
      .expect(402, done);
  });

  it("Validation: is a none object payload", (done) => {
    request
      .post("/api/estimate")
      .send([])
      .expect("Content-Type", /json/u)
      .expect(JSON.stringify({ error: '"value" must be of type object' }))
      .expect(402, done);
  });

  it("Validation: is a none numeric value", (done) => {
    request
      .post("/api/estimate")
      .send({ users: "sas" })
      .expect("Content-Type", /json/u)
      .expect(JSON.stringify({ error: '"users" must be a number' }))
      .expect(402, done);
  });

  it("Should Return the correct estimation requirement", (done) => {
    const payload = {
      users: 200,
      memory: 16,
      userMemory: 128,
      redundancy: 2,
    };
    const expectedResp = {
      message: "Basic Memory Estimation Requirement",
      advice: "A minimun of 4 CPUs and 16GB memory per server is required.",
      data: {
        requiredMemory: "32.50",
        cpu: 4,
        growthPercent: 1.3,
        servers: 5,
        minServerMemory: 16,
      },
    };
    request
      .post("/api/estimate")
      .send(payload)
      .expect("Content-Type", /json/u)
      .expect(expectedResp)
      .expect(200, done);
  });
  it("Should return estimate with default values", (done) => {
    const payload = { users: 400 };
    const expectedResp = {
      requiredMemory: "65.00",
      servers: 9,
      minServerMemory: 16,
      cpu: 4,
      growthPercent: 1.3,
    };
    request
      .post("/api/estimate")
      .send(payload)
      .expect("Content-Type", /json/u)
      .expect((res) => {
        const respose = res.body.data;
        assert.deepStrictEqual(respose, expectedResp);
      })
      .expect(200, done);
  });
});
