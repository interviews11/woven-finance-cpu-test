# Woven Finance

## SERVER PLANNER SERVICE                                         

[#emmsdan engine#] http://woven.labs.emmsdan.com.ng/               

### Write an API that calculates the server hardware for a data center (to host virtual machines).

### Your API should return the number of servers that are required to host a non-empty collection of virtual machines.

### This means you should implement the following operation (in UML definition): 

 ```java 
 calculate(serverType: Server, virtualMachines: VirtualMachin[1..*]): int
 ```

### All servers are from the same type. This type defines the hardware resources each server provides: CPU, RAM, HDD.

### Each virtual machine is defined by the virtual hardware resources it needs (on a server): CPU, RAM, HDD.


#### Run the docker image
```bash
docker run -p 7003:7003 -d registry.gitlab.com/interviews11/woven-finance-cpu-test
```

## INSTALLATION

Clone Repo and cd 
```bash
git clone git@gitlab.com:interviews11/woven-finance-cpu-test.git ./woven

cd woven
```
```

Install dependencies

### Option 1

```bash
yarn install 
```


## RUN APP

### As Development
```bash
yarn dev // with live reload
```

### As production
```bash
yarn build // to build file
yarn start // to run app
```

### visit app on
```url
http://localhost:7003
```

### Option 2

#### Build docker image
```bash
docker build -t emmsdan/woven-test .
```


#### Run the docker image
```bash
docker run -p 7003:7003 -d emmsdan/woven-test
```