FROM node

WORKDIR /usr/src/

COPY package*.json ./
COPY yarn.lock ./

RUN yarn install

COPY . ./

RUN yarn ci

EXPOSE 7003
CMD [ "node", "dist/server.js" ]